from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='baobaxia',
    version='0.1.2',
    author='NPDD/Rede Mocambos',
    author_email='npdd@mocambos.net',
    description='Baobáxia is an eventually connected data network',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://labmocambos.taina.net.br/npdd/baobaxia-mucua',
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    packages=find_packages(include=['baobaxia', 'baobaxia.*']),
    install_requires=[
        'starlette',
        'pydantic[email,dotenv]',
        'uvicorn[standard]',
        'fastapi',
        'python-multipart',
        'aiofiles',
        'python-slugify',
        'datalad',
        'shortuuid',
    ],
    entry_points={'console_scripts':'criar_mucua=baobaxia.install:criar_mucua'},
    setup_requires=['pytest-runner'],
    test_requires=['pytest','nose']

)
