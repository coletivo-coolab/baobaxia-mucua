#!/bin/bash   

# Pasta do  projeto
PROJECTDIR=/home/mocambola/baobaxia-dev/
# Api instance module
ASGI_MODULE=baobaxia.acervo
PORT=8080
# Pode usar o socket no caso (con NGINX)
SOCKFILE=/home/mocambola/baobaxia-dev/run/unicorn_live.sock


# the user to run as
USER=mocambola
# the group to run as                                            
GROUP=mocambola

# Number of Workers
NUM_WORKERS=1

WORKER_CLASS=uvicorn.workers.UvicornWorker

TIMEOUT=300
GRACEFUL_TIMEOUT=120
LOG_DIR=log
LOGFILE=/home/mocambola/baobaxia-dev/log/bbx.log
MAX_REQUESTS=50000
MAX_REQUEST_JITTER=4

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $PROJECTDIR
source ./bin/activate

# Create the run and log directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR
test -d $PROJECTDIR/$LOGDIR || mkdir -p $LOGDIR


# Start your Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec ../.local/bin/gunicorn ${ASGI_MODULE}:api \
    --worker-class $WORKER_CLASS \
    --workers $NUM_WORKERS \
    --timeout $TIMEOUT \
    --user=$USER \
    --group=$GROUP \
    --bind=0.0.0.0:8080 \
    --log-level=debug \
    --log-file=$LOGFILE \
    --max-requests=$MAX_REQUESTS \
    --graceful-timeout=$GRACEFUL_TIMEOUT
    --max-requests-jitter=$MAX_REQUEST_JITTER
#    --bind=unix:$SOCKFILE \
