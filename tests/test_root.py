from pathlib import Path

from .test_env import BaseBaobaxiaTest, DataTestModel

from baobaxia.root import Baobaxia


class TestBaobaxia(BaseBaobaxiaTest):

    def token(self, baobaxia):
        return baobaxia.authenticate(
            username=self.mocambola_username,
            password=self.mocambola_password
        )

    def test_list_balaios(self):
        baobaxia = Baobaxia(self.config)
        token = self.token(baobaxia)
        balaios_list = baobaxia.list_balaios(token)
        assert len(balaios_list) > 0

    def test_get_balaio(self):
        baobaxia = Baobaxia(self.config)
        token = self.token(baobaxia)
        balaio = baobaxia.get_balaio(self.balaio_slug, token)
        assert balaio.smid == self.balaio_smid
        assert balaio.name == self.balaio_name

    def test_put_balaio(self):
        baobaxia = Baobaxia(self.config)
        token = self.token(baobaxia)
        balaio = baobaxia.get_balaio(self.balaio_slug, token)
        old_name = balaio.name
        new_name = "Nome alterado"
        balaio = baobaxia.put_balaio(
            slug = self.balaio_slug,
            name = new_name,
            token = token)
        assert balaio.name == new_name
        balaio = baobaxia.put_balaio(
            slug = self.balaio_slug,
            name = old_name,
            token = token)
        assert balaio.name == old_name
        new_name = "Novo balaio"
        balaio = baobaxia.put_balaio(
            name = new_name,
            token = token)
        assert balaio.slug is not None
        assert balaio.name == new_name

    def test_del_balaio(self):
        baobaxia = Baobaxia(self.config)
        token = self.token(baobaxia)
        balaio_name = "Balaio para deletar"
        balaio = baobaxia.put_balaio(
            name = balaio_name,
            token = token)
        balaio_slug = balaio.slug
        baobaxia.del_balaio(
            balaio_slug,
            token)
        try:
            baobaxia.get_balaio(balaio_slug)
        except:
            pass
        else:
            assert False

    def test_list_mucuas(self):
        baobaxia = Baobaxia(self.config)
        token = self.token(baobaxia)
        mucuas = baobaxia.list_mucuas(
            balaio_slug = self.balaio_slug,
            token = token)
        assert len(mucuas) > 0

    def test_get_mucua(self):
        baobaxia = Baobaxia(self.config)
        token = self.token(baobaxia)
        mucua = baobaxia.get_mucua(
            balaio_slug = self.balaio_slug,
            mucua_slug = self.mucua_slug,
            token = token)
        mucua.smid = self.mucua_smid
        mucua.name = self.mucua_name


    def test_put_mucua(self):
        baobaxia = Baobaxia(self.config)
        token = self.token(baobaxia)
        mucua = baobaxia.get_mucua(
            balaio_slug = self.balaio_slug,
            mucua_slug = self.mucua_slug,
            token = token)
        old_name = mucua.name
        new_name = "Nome alterado"
        mucua = baobaxia.put_mucua(
            balaio_slug = self.balaio_slug,
            mucua_slug = self.mucua_slug,
            name = new_name,
            token = token)
        assert mucua.name == new_name
        mucua = baobaxia.put_mucua(
            balaio_slug = self.balaio_slug,
            mucua_slug = self.mucua_slug,
            name = old_name,
            token = token)
        assert mucua.name == old_name
        new_name = "Nova mucua"
        mucua = baobaxia.put_mucua(
            balaio_slug = self.balaio_slug,
            name = new_name,
            token = token)
        assert mucua.slug is not None
        assert mucua.name == new_name

    def test_del_mucua(self):
        baobaxia = Baobaxia(self.config)
        token = self.token(baobaxia)
        mucua_name = "Mucua para deletar"
        mucua = baobaxia.put_mucua(
            balaio_slug = self.balaio_slug,
            name = mucua_name,
            token = token)
        mucua_slug = mucua.slug
        baobaxia.del_mucua(
            balaio_slug = self.balaio_slug,
            mucua_slug = mucua_slug,
            token = token)
        try:
            baobaxia.get_mucua(
                balaio_slug = self.balaio_slug,
                mucua_slug = mucua_slug,
                token = token)
        except:
            pass
        else:
            assert False

    def test_discover_saberes(self):
        test_dir = self.config.data_path / \
            self.balaio_slug / self.mucua_slug / 'test'
        if not test_dir.exists():
            test_dir.mkdir()

        baobaxia = Baobaxia(self.config)
        token = self.token(baobaxia)
        baobaxia.discover_saberes(
            balaio_slug = self.balaio_slug,
            mucua_slug = self.mucua_slug,
            model = DataTestModel,
            patterns = ['test/*'])
        assert hasattr(baobaxia, 'list_datatestmodels')
        assert hasattr(baobaxia, 'get_datatestmodel')
        assert hasattr(baobaxia, 'put_datatestmodel')
        assert hasattr(baobaxia, 'del_datatestmodel')

        test_name = 'meu_teste'
        test_path = Path('test/meu_teste')
        test_message = 'Isto é um teste'
        test_saber = baobaxia.put_datatestmodel(
            path = test_path,
            name = test_name,
            data = DataTestModel(message = test_message),
            token = token)
        test_smid = test_saber.smid

        baobaxia = Baobaxia(self.config)
        token = self.token(baobaxia)
        baobaxia.discover_saberes(
            balaio_slug = self.balaio_slug,
            mucua_slug = self.mucua_slug,
            model = DataTestModel,
            patterns = ['test/*'],
            field_name = 'test',
            list_field_name = 'tests_collection')

        tests = baobaxia.list_tests_collection(token)
        assert len(tests) > 0
        test_saber = baobaxia.get_test(
            path = test_path,
            token = token)
        assert test_smid == test_saber.smid
        assert test_name == test_saber.name
        assert test_message == test_saber.data.message

        baobaxia.del_test(
            path = test_path,
            token = token)

