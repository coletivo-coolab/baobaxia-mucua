import os
import pytest
from pathlib import Path
from datetime import datetime

from datalad.api import create
from pydantic import BaseModel

from baobaxia.saberes import (
    Saber,
    Balaio,
    Mucua,
    Mocambola,
    SaberesConfig
)

class DataTestModel(BaseModel):
    message: str
    any_int: int = -1

class BaseBaobaxiaTest():

    data_path = Path('/tmp/baobaxia')
    mocambola_smid = '1357'
    mocambola_username = 'exu'
    mocambola_slug = mocambola_username + '-' + mocambola_smid
    mocambola_email = 'exu@mocambos.net'
    mocambola_password = 'livre123'
    default_date = datetime(2021, 3, 29, 0, 0, 0, 0)
    mocambola = Mocambola(
        username = mocambola_username,
        email = mocambola_email
    )

    balaio_name = 'meu-balaio'
    balaio_smid = '123456789101112'
    balaio_slug = balaio_name + '_' + balaio_smid
    balaio_path = data_path / balaio_slug
    balaio_filepath = balaio_path / '.baobaxia'
    balaio_saber = Saber(
        path = Path(balaio_slug),
        smid = balaio_smid,
        name = balaio_name,
        slug = balaio_slug,
        data = Balaio(),
        creator = mocambola,
        created = default_date,
        last_update = default_date
    )

    outro_balaio_name = 'rede-mocambos'
    outro_balaio_smid = '892xa89sajka'
    outro_balaio_slug = outro_balaio_name + '_' + outro_balaio_smid
    outro_balaio_path = data_path / outro_balaio_slug
    outro_balaio_filepath = outro_balaio_path / '.baobaxia'
    outro_balaio_saber = Saber(
        path = Path(outro_balaio_slug),
        smid = outro_balaio_smid,
        name = outro_balaio_name,
        slug = outro_balaio_slug,
        data = Balaio(),
        creator = mocambola,
        created = default_date,
        last_update = default_date
    )

    mucua_name = 'abdias'
    mucua_smid = '21921921919291'
    mucua_slug = mucua_name + '-' + mucua_smid
    mucua_path = balaio_path / mucua_slug
    mucua_filepath = mucua_path / '.baobaxia'
    mucua_saber = Saber(
        path = mucua_slug,
        smid = mucua_smid,
        name = mucua_name,
        slug = mucua_slug,
        balaio = balaio_slug,
        data = Mucua(),
        creator = mocambola,
        created = default_date,
        last_update = default_date
    )

    outro_balaio_mucua_path = outro_balaio_path / mucua_slug
    outro_balaio_mucua_filepath = outro_balaio_mucua_path / '.baobaxia'

    outro_balaio_mucua_saber = Saber(
        path = mucua_slug,
        smid = mucua_smid,
        name = mucua_name,
        slug = mucua_slug,
        balaio = outro_balaio_slug,
        data = Mucua(),
        creator = mocambola,
        created = default_date,
        last_update = default_date
    )
    
    mocambolas_path = mucua_path / 'mocambolas'
    mocambola_path = mocambolas_path / mocambola_slug
    mocambola_filepath = mocambola_path / '.baobaxia'
    mocambola_saber = Saber(
        path = mocambola_path,
        smid = mocambola_smid,
        name = mocambola_username,
        slug = mocambola_slug,
        balaio = balaio_slug,
        mucua = mucua_slug,
        data = mocambola,
        creator = mocambola,
        created = default_date,
        last_update = default_date
    )

    saber_dir_name = 'saudacoes'
    saber_dir_data = DataTestModel(message = 'Saudações')
    saber_file_name1 = 'alomundo.txt'
    saber_file_data1 = DataTestModel(message = 'Olá, Mundo!!!')
    saber_file_name2 = 'ciaomondo.txt'
    saber_file_data2 = DataTestModel(message = 'Ciao, Mundo!!!')
    saber_fileannex_name1 = 'aloannex.bin'
    saber_fileannex_data1 = DataTestModel(message = 'Olá, Mundo!!!')
    saber_fileannex_balaio = outro_balaio_slug
    saber_sluged_name = 'algo_importante'
    saber_sluged_data = DataTestModel(
        message = 'Algo importante está guardado aqui!',
        any_int =132435
    )
    nova_mucua_slug = 'abdias-nascimento_LZsgH'
    nova_mucua_remote = 'ssh://exu@adbias.mocambos.net:2067/data/bbx/balaios/acervo-patrimonio-red_jNXhc'

    
    config = SaberesConfig(
        data_path = data_path,
        balaio_local = balaio_slug,
        mucua_local = mucua_slug
    )


    def init_balaio_local(self):
        # BALAIO de base
        if not self.data_path.exists():
            self.data_path.mkdir()

        if not self.balaio_path.exists():
            self.balaio_path.mkdir()

        if not self.balaio_filepath.exists():
            self.balaio_filepath.open('w').write(
                self.balaio_saber.json())

        if not self.mucua_path.exists():
            self.mucua_path.mkdir()

        if not self.mucua_filepath.exists():
            self.mucua_filepath.open('w').write(
                self.mucua_saber.json())

        if not self.mocambolas_path.exists():
            self.mocambolas_path.mkdir()

        if not self.mocambola_path.exists():
            self.mocambola_path.mkdir()

        if not self.mocambola_filepath.exists():
            from baobaxia.util import str_to_hash
            self.mocambola_saber.data = self.mocambola_saber.data.copy()
            self.mocambola_saber.data.password_hash = str_to_hash(
                self.mocambola_password)
            self.mocambola_filepath.open('w').write(
                self.mocambola_saber.json())

        # Inicializa um repositorio git temporariamente e adiciona no
        # .git/config o UUID da Mucua Local 
        create(path=self.balaio_path, annex=False, force=True)
        gitconfig = self.balaio_path / '.git'/ 'config'
        os.remove(self.balaio_path / '.noannex')
        with open(gitconfig, "a") as f:
            f.write("[annex]\n")
            f.write("\tuuid = "+str(self.mucua_smid)+"\n")
        # Agora reinicializa de verdade com os parametros certos.
        dataset = create(path=self.balaio_path,
                         force=True,
                         annex=True,
                         cfg_proc='text2git',
                         description=self.mucua_slug)
        dataset.save()

        # Outro OUTRO_BALAIO
        if not self.outro_balaio_path.exists():
            self.outro_balaio_path.mkdir()

        if not self.outro_balaio_filepath.exists():
            self.outro_balaio_filepath.open('w').write(
                self.outro_balaio_saber.json())

        if not self.outro_balaio_mucua_path.exists():
            self.outro_balaio_mucua_path.mkdir()

        if not self.mucua_filepath.exists():
            self.mucua_filepath.open('w').write(
                self.mucua_saber.json())

        # Inicializa um repositorio git temporariamente e adiciona no
        # .git/config o UUID da Mucua Local 
        create(path=self.outro_balaio_path, annex=False, force=True)
        gitconfig = self.outro_balaio_path / '.git'/ 'config'
        os.remove(self.outro_balaio_path / '.noannex')
        with open(gitconfig, "a") as f:
            f.write("[annex]\n")
            f.write("\tuuid = "+str(self.mucua_smid)+"\n")
        # Agora reinicializa de verdade com os parametros certos.
        dataset = create(path=self.outro_balaio_path,
                         force=True,
                         annex=True,
                         cfg_proc='text2git',
                         description=self.mucua_slug)
        dataset.save()
        
        self.create_sample_content()

    def create_sample_content(self):
        testfile1 = self.mucua_path / self.saber_file_name1
        testfile2 = self.mucua_path / self.saber_file_name2
        testfileannex1 = self.outro_balaio_mucua_path / self.saber_fileannex_name1
        
        if not testfile1.exists():
            print("Criando: "+ str(testfile1))
            Path(testfile1).touch()
        if not testfile2.exists():
            print("Criando: "+ str(testfile2))
            Path(testfile2).touch()
        if not testfileannex1.exists():
            print("Criando file annex: "+ str(testfileannex1))
            with open(testfileannex1, 'wb') as fb:
                fb.write(os.urandom(1024))
            
        testfolder = self.mucua_path / self.saber_dir_name
        if not testfolder.exists():
            print("Criando pasta: "+ str(testfolder))
            testfolder.mkdir()



