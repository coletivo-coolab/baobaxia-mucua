from baobaxia.root import Mocambola
from baobaxia.saberes import Balaio, Mucua, SaberesDataStore
from baobaxia.sankofa import Sankofa

from .test_env import BaseBaobaxiaTest, DataTestModel
from pathlib import Path

from pydantic import BaseModel
from datalad.tests.utils import ok_file_under_git

class TestSankofa(BaseBaobaxiaTest):

    def test_add(self):
        datastore = SaberesDataStore(self.config)

        dataset = datastore.create_dataset(
            mocambola = self.mocambola,
            balaio = self.balaio_slug,
            mucua = self.mucua_slug,
            model = DataTestModel)

        outro_dataset = datastore.create_dataset(
            mocambola = self.mocambola,
            balaio = self.outro_balaio_slug,
            mucua = self.mucua_slug,
            model = DataTestModel)
       
        self.saber_file1 = dataset.settle_saber(                          
            path = self.saber_file_name1,
            name = self.saber_file_name1,
            data = self.saber_file_data1)                                                                                                                     
        self.saber_file2 = dataset.settle_saber(           
            path = self.saber_file_name2,                                         
            name = self.saber_file_name2,                   
            data = self.saber_file_data2)
        
        self.saber_file3 = outro_dataset.settle_saber(          
            path = self.saber_fileannex_name1,                     
            name = self.saber_fileannex_name1,                     
            data = self.saber_fileannex_data1,
        ) 
        
        Sankofa.add(saberes=[self.saber_file1,
                             self.saber_file2,
                             self.saber_file3],
                    mocambola=self.mocambola,
                    config=self.config)
        
        assert ok_file_under_git(
            self.config.data_path / self.saber_file1.balaio,
            str(self.saber_file1.mucua / self.saber_file1.path)) == None

        assert ok_file_under_git(
            self.config.data_path / self.saber_file2.balaio,
            str(self.saber_file2.mucua / self.saber_file2.path)) == None

        assert ok_file_under_git(
            self.config.data_path / self.saber_file3.balaio,
            str(self.saber_file3.mucua / self.saber_file3.path),
            annexed=True) == None

    def test_remove(self):
        datastore = SaberesDataStore(self.config)

        dataset = datastore.create_dataset(
            mocambola = self.mocambola,
            balaio = self.balaio_slug,
            mucua = self.mucua_slug,
            model = DataTestModel)

        outro_dataset = datastore.create_dataset(
            mocambola = self.mocambola,
            balaio = self.outro_balaio_slug,
            mucua = self.mucua_slug,
            model = DataTestModel)
        
        self.saber_file1 = dataset.collect_saber(                                                                                                             
            path = self.saber_file_name1)
        # self.saber_file3 = outro_dataset.collect_saber(
        #     path = self.saber_fileannex_name1)
        
        Sankofa.remove(saberes=[self.saber_file1],
                       mocambola=self.mocambola,
                       config=self.config)

        sf1 = self.config.data_path / self.saber_file1.balaio / \
            self.saber_file1.mucua / self.saber_file1.path
        
        assert not sf1.exists()

        # sf3 = self.config.data_path / self.saber_file3.balaio / \
        #     self.saber_file3.mucua / self.saber_file3.path

        # assert not sf3.exists()

    def test_sync(self):

        Sankofa.sync(balaio_slug=self.balaio_slug, config=self.config)


    def test_mucua_remote_add_and_get(self):

        Sankofa.mucua_remote_add(balaio_slug=self.balaio_slug,
                                 mucua_slug=self.nova_mucua_slug,
                                 uri=self.nova_mucua_remote,
                                 config=self.config)

        remotes = Sankofa.mucua_remote_get(balaio_slug=self.balaio_slug,
                                            config=self.config)
        
        assert self.nova_mucua_remote in remotes

        
    def test_mucua_remote_del(self):

        Sankofa.mucua_remote_del(balaio_slug=self.balaio_slug,
                                    mucua_slug=self.nova_mucua_slug,
                                    config=self.config)

        remotes = Sankofa.mucua_remote_get(balaio_slug=self.balaio_slug,
                                  config=self.config)
        
        assert not remotes 

    def test_mucua_group_add_and_get(self):

        novo_territorio = 'territorio-taina'
        
        Sankofa.mucua_group_add(balaio_slug=self.balaio_slug,
                                mucua_slug=self.mucua_slug,
                                group=novo_territorio,
                                config=self.config)

        groups = Sankofa.mucua_group_list(balaio_slug=self.balaio_slug,
                                  mucua_slug=self.mucua_slug,
                                  config=self.config)
        
        assert novo_territorio in groups

    def test_mucua_group_del(self):

        novo_territorio = 'territorio-taina'
        
        Sankofa.mucua_group_del(balaio_slug=self.balaio_slug,
                                mucua_slug=self.mucua_slug,
                                group=novo_territorio,
                                config=self.config)

        groups = Sankofa.mucua_group_list(balaio_slug=self.balaio_slug,
                                  mucua_slug=self.mucua_slug,
                                  config=self.config)
        
        assert not groups

                                 
                                 
                                 
