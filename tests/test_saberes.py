from baobaxia.saberes import Balaio, Mucua, Mocambola, SaberesDataStore
from baobaxia.sankofa import Sankofa
from .test_env import BaseBaobaxiaTest, DataTestModel
from pathlib import Path

from pydantic import BaseModel

class TestSaberes(BaseBaobaxiaTest):

    def test_list_balaios(self):
        datastore = SaberesDataStore(self.config)
        dataset = datastore.create_dataset(
            mocambola=self.mocambola,
            model=Balaio)
        balaios_list = dataset.collect_saberes('*/')
        assert len(balaios_list) > 0
        balaio_local = None
        for balaio in balaios_list:
            if balaio.smid == self.balaio_smid:
                balaio_local = balaio
                break
        assert balaio_local.slug == self.balaio_slug
        assert balaio_local.name == self.balaio_name

    def test_list_mucuas(self):
        datastore = SaberesDataStore(self.config)
        dataset = datastore.create_dataset(
            mocambola=self.mocambola,
            model=Mucua,
            balaio=self.balaio_slug)
        mucuas_list = dataset.collect_saberes('*/')
        assert len(mucuas_list) > 0
        mucua_local = None
        for mucua in mucuas_list:
            if mucua.smid == self.mucua_smid:
                mucua_local = mucua
                break
        assert mucua_local.slug == self.mucua_slug
        assert mucua_local.name == self.mucua_name

    def test_create_dir(self):
        datastore = SaberesDataStore(self.config)
        dataset = datastore.create_dataset(
            mocambola=self.mocambola,
            model=DataTestModel,
            balaio=self.balaio_slug,
            mucua=self.mucua_slug)
        self.saber_dir_path = self.mucua_path / self.saber_dir_name
        if not self.saber_dir_path.exists():
            self.saber_dir_path.mkdir()
        self.saber_dir = dataset.settle_saber(
            path = self.saber_dir_name,
            name = self.saber_dir_name,
            data = self.saber_dir_data)

        assert len(self.saber_dir.slug) == \
            self.config.slug_name_len + \
            self.config.slug_smid_len + \
            len(self.config.slug_sep)

        assert self.saber_dir.data.__class__ == DataTestModel

        self.saber_dir_filepath = self.saber_dir_path / '.baobaxia'
        assert self.saber_dir_filepath.is_file()

    def test_create_files(self):
        datastore = SaberesDataStore(self.config)
        dataset = datastore.create_dataset(
            mocambola = self.mocambola,
            balaio = self.balaio_slug,
            mucua = self.mucua_slug,
            model = DataTestModel)
        self.saber_file1 = dataset.settle_saber(
            path = self.saber_file_name1,
            name = self.saber_file_name1,
            data = self.saber_file_data1)
        self.saber_file2 = dataset.settle_saber(
            path = self.saber_file_name2,
            name = self.saber_file_name2,
            data = self.saber_file_data2)

        assert len(self.saber_file1.slug) == \
            self.config.slug_name_len + \
            self.config.slug_smid_len + \
            len(self.config.slug_sep)

        self.saber_file_path1 = self.mucua_path / (
            self.saber_file_name1 + '.baobaxia')
        assert self.saber_file_path1.is_file()

        assert len(self.saber_file2.slug) == \
            self.config.slug_name_len + \
            self.config.slug_smid_len + \
            len(self.config.slug_sep)

        self.saber_file_path2 = self.mucua_path / (
            self.saber_file_name2 + '.baobaxia')
        assert self.saber_file_path2.is_file()

    def test_create_sluged(self):
        datastore = SaberesDataStore(self.config)
        dataset = datastore.create_dataset(
            mocambola = self.mocambola,
            balaio = self.balaio_slug,
            mucua = self.mucua_slug,
            model = DataTestModel)
        self.saber_sluged = dataset.settle_saber(
            path = Path('.'),
            name = self.saber_sluged_name,
            data = self.saber_sluged_data,
            slug_dir = True)

        assert len(self.saber_sluged.slug) == \
            self.config.slug_name_len + \
            self.config.slug_smid_len + \
            len(self.config.slug_sep)

        self.saber_sluged_path = self.mucua_path / self.saber_sluged.slug
        assert self.saber_sluged_path.is_dir()

        self.saber_sluged_filepath = self.saber_sluged_path / '.baobaxia'
        assert self.saber_sluged_filepath.is_file()

    def test_update_saberes(self):
        datastore = SaberesDataStore(self.config)
        dataset = datastore.create_dataset(
            mocambola = self.mocambola,
            balaio = self.balaio_slug,
            mucua = self.mucua_slug,
            model = DataTestModel)

        self.saber_file1 = dataset.settle_saber(
            path = self.saber_file_name1,
            name = self.saber_file_name1,
            data = self.saber_file_data1)

        new_data = DataTestModel(message = "Conteúdo alterado", any_int = 31313131)
        new_saber = dataset.settle_saber(
            path = self.saber_file_name1,
            name = self.saber_file_name1,
            data = new_data)

        assert self.saber_file1.data.message != new_saber.data.message
        assert self.saber_file1.data.any_int != new_saber.data.any_int

        new_saber = dataset.settle_saber(
            path = self.saber_file_name1,
            name = self.saber_file_name1,
            data = self.saber_file_data1)

        assert self.saber_file1.data.message == new_saber.data.message
        assert self.saber_file1.data.any_int == new_saber.data.any_int

        self.saber_dir_path = self.mucua_path / self.saber_dir_name
        if not self.saber_dir_path.exists():
            self.saber_dir_path.mkdir()
        self.saber_dir = dataset.settle_saber(
            path = self.saber_dir_name,
            name = self.saber_dir_name,
            data = self.saber_dir_data)

        new_saber = dataset.settle_saber(
            path = self.saber_dir_name,
            name = self.saber_dir_name,
            data = new_data)

        assert self.saber_dir.data.message != new_saber.data.message
        assert self.saber_dir.data.any_int != new_saber.data.any_int

        new_saber = dataset.settle_saber(
            path = self.saber_dir_name,
            name = self.saber_dir_name,
            data = self.saber_dir_data)

        assert self.saber_dir.data.message == new_saber.data.message
        assert self.saber_dir.data.any_int == new_saber.data.any_int

    def test_delete_saberes(self):
        datastore = SaberesDataStore(self.config)
        dataset = datastore.create_dataset(
            mocambola = self.mocambola,
            balaio = self.balaio_slug,
            mucua = self.mucua_slug,
            model = DataTestModel)

        self.saber_file_path1 = self.mucua_path / (
            self.saber_file_name1 + '.baobaxia')
        self.saber_dir_path = self.mucua_path / self.saber_dir_name
        if not self.saber_dir_path.exists():
            self.saber_dir_path.mkdir()
        self.saber_dir_filepath = self.saber_dir_path / '.baobaxia'


        self.saber_file1 = dataset.settle_saber(
            path = self.saber_file_name1,
            name = self.saber_file_name1,
            data = self.saber_file_data1)

        assert self.saber_file_path1.is_file()

        Sankofa.drop(saberes=[self.saber_file1],
                     mocambola=self.mocambola,
                     config=self.config)

        assert not self.saber_file_path1.exists()


        self.saber_dir = dataset.settle_saber(
            path = self.saber_dir_name,
            name = self.saber_dir_name,
            data = self.saber_dir_data)

        assert self.saber_dir_filepath.is_file()

        dataset.drop_saber(self.saber_dir.path)

        assert not self.saber_dir_filepath.exists()
