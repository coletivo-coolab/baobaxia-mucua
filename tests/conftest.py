import pytest
from .test_env import BaseBaobaxiaTest

@pytest.fixture(autouse=True, scope='session')
def base():
    b = BaseBaobaxiaTest()
    b.init_balaio_local()
