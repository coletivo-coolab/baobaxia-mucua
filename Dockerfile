FROM debian:bullseye

ARG BALAIO='Rede Mocambos Acervo'
ARG EMAIL='mocambola@mocambos.net'
ARG MOCAMBO='Casa de Cultura Tainã'
ARG MOCAMBOLA='mocambola'
ARG MUCUA='Janeraka'
ARG SENHA='livre'
ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1

RUN apt-get update && apt-get install -y \
    build-essential \
    libffi-dev \
    libssl-dev \
    python3-dev \
    python3-pip \
    cargo \
    git-annex


RUN pip3 install --upgrade pip
COPY requirements.txt /
COPY requirements/* /requirements/

RUN pip3 install cryptography --no-binary cryptography
RUN pip3 install -r /requirements.txt
RUN pip3 install git+https://github.com/datalad/datalad

EXPOSE 80

COPY ./baobaxia /baobaxia
RUN mkdir -p data/bbx/balaios

RUN git config --global user.email "$EMAIL"
RUN git config --global user.name "$MUCUA"

RUN criar_mucua --path='/data/bbx/balaios' \
    --balaio "$BALAIO" \
    --mucua "$MUCUA" \
    --mocambo "$MOCAMBO" \
    --mocambola "$MOCAMBOLA" \
    --email "$EMAIL" \
    --password "$SENHA" \
    --smid_len 13 \
    --slug_name_len 21 \
    --slug_smid_len 5 \
    --slug_sep "_"

ENTRYPOINT ["uvicorn", "baobaxia.acervo:api", "--host", "0.0.0.0", "--port", "80"]
